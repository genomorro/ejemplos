#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int help() {
  puts ("Uso del programa: <opciones> [sección]\n");
  puts ("                  [sección]");
  return 0;
}

int search(char *svalue) {
  printf ("Se buscará el término '%s' en el programa.\n", svalue);
  return 0;
}


int main(int argc, char *argv[])
{
  int opt;
  int index;
  
  if (argc < 2)
  {
    help ();
    return -1;
  }

  while ((opt = getopt(argc, argv, "hs:")) != -1) 
    switch (opt)
      {
      case 'h':
	help();
	break;
      case 's':
	search (optarg);
	break;
      default:
	help();
      }

  if(optind != argc)
    {
      printf("Otras instrucciones proporcionadas: \n");
      printf("Argumento \t Valor\n");
      for (index = optind; index < argc ; index++)
	printf ("argv[%i] \t %s \n", index, argv[index]);       
    }
  
  printf ("\n");
  return 0;
}


