#include <stdio.h>
    
int main(int argc, char *argv[])
{
  int i;
  printf("Argumento \t Valor \n");
  for (i=0; i<argc; i++) {
    printf("argv[%i] \t %s \n",i, argv[i]);
  }
  printf("\n");
  return 0;
}
