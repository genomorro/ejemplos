# Ejemplos

Ejemplos de programación para alumnos:

- argumentos.c: muestra la estructura del arreglo argv[]
- argumentosTabla.c: imprime los argumentos de un programa en una tabla
- parce.c: Manejo de argumentos con getopt()
- lista_doble.c: Cómo hacer una lista doblemente enlazada
