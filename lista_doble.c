/*
 * Lista doblemente enlazada.
 */

/* 
 * NOTAS: Debes solo hice los fáciles :) Debes implementar como buscar
 * un nodo específico. Luego como borrar e insertar en ese lugar. Yo
 * hice que las funciones devolvieran int, lo correcto es que utilices
 * true y false con bool.h. Finalmente debes ingresar los datos de
 * forma interactiva, MODIFICA ESO AL FINAL, te será más sencillo ver
 * los errores y llevar un orden.
*/

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

/*
 * Crear la estructura: Debe tener un dato, un apuntador al nodo
 * siguiente y un apuntador al nodo anterior. No hice un typedef
 * porque no me intereso, si lo quieres así puedes implementarlo una
 * vez que completes todos los algoritmos
 */
struct _nodo {
  int64_t dato;
  struct _nodo *siguiente;
  struct _nodo *anterior;
};

/*
 * Este apuntador señala siempre al nodo inicial, muchas funciones lo
 * piden por lo que al ser global me ahorro un argumento.  Esto me
 * ahorra también el contador, la desventaja es que debo recorrer la
 * lista completa siempre que lo use.
 */
struct _nodo *inicio;

/*
 * Crea un nodo y regesa el nodo (el apuntador al nodo). La función
 * recibe un entero pues en la estructura el dato es tipo entero
 */
struct _nodo *crearNodo(int x) {
  /*
   * Con calloc digo que alojaré un elemento del tamaño del nodo Tu lo
   * harías como en tu código anterior, yo puse calloc porque parece que
   * es más preciso, tu harías:
   * struct _nodo *nuevo = (struct _nodo*)malloc(sizeof(struct _nodo));
   */
  struct _nodo *nuevo = calloc(1,sizeof(struct _nodo));
  /* 
   * Aquí pongo el valor de x en el dato de la estructura
   */
  nuevo->dato = x;
  /*
   * Dejo los apuntadores del nodo vacios porque los ligaré después
   */
  nuevo->anterior = NULL;
  nuevo->siguiente = NULL;
  return nuevo;
}

/* 
 * Inserta un nodo al inicio de la lista Regresa 0 si fue exitosa la
 * operación.
 */
int64_t insertaInicio(int x) {
  /*
   * Primero creo el nodo que insertaré.
   */
  struct _nodo *nuevo = crearNodo(x);
  /*
   * Si la lista esta vacia el nodo nuevo se convierte en el nodo inicio
   */
  if (inicio == NULL) {
    inicio = nuevo;
    return 0;
  }
  /*
   * Si no esta vacia la lista entonces se direccionan los apuntadores 
   */
  inicio->anterior = nuevo;
  nuevo->siguiente = inicio;
  /*
   * Identifica al nuevo nodo como el inicio
   */
  inicio = nuevo;
  return 0;
}

/*
 * Inserta un nodo al final de la lista Regresa 0 si fue existosa la
 * operación
 */
int64_t insertaFinal(int x) {
  /*
   * Copia inicio en una posición auxiliar 
   */
  struct _nodo *aux = inicio;
  struct _nodo *nuevo = crearNodo(x);
  /* 
   * Si la lista esta vacía el nodo nuevo se convierte en inicio 
   */
  if(inicio == NULL) {
    inicio = nuevo;
    return 0;
  }
  /* 
   * Recorremos la lista del primer nodo hasta el final
   */
  while(aux->siguiente != NULL) {
    aux = aux->siguiente;
  }
  /*
   * Al llegar al final ajustamos apuntadores
   */
  aux->siguiente = nuevo;
  nuevo->anterior = aux;
  return 0;
}

/*
 * Borra el nodo de inicio, si es exitoso regresa 0
 */
int64_t borrarInicio() {
  /*
   * Copia el nodo inicio al auxiliar borrar
   */
  struct _nodo *borrar = inicio;
  /*
   * Si la lista esta vacia no hacer nada
   */
  if(borrar == NULL){
    return 0;
  }
  /*
   * Si la lista tiene más de un elemento entonces se recorre el
   * inicio y se establece un apuntador a NULL
   */
  if(borrar->siguiente != NULL) {
    inicio = inicio->siguiente;
    inicio->anterior = NULL;
    /*
     * No olvides la opreación de borrar el nodo que no quieres
     */
    free(borrar);
    return 0;
  }
  /*
   * Si la lista solo tiene un nodo, entonces simplemente reiniciamos
   * la lista.
   */
  inicio = NULL;
  free(borrar);
  return 0;
}

/*
 * Borrar el nodo final, si es exitoso regresa 0
 */
int64_t borrarFinal() {
  struct _nodo *borrar = inicio;
  /*
   * Si la lista esta vacia no hacer nada
   */
  if(borrar == NULL){
    return 0;
  }
  /*
   * Recorremos la lista del primer nodo hasta el final
   */
  while(borrar->siguiente != NULL) {
    borrar = borrar->siguiente;
  }
  /*
   * Si la lista tiene más de un elemento
   */
  if(borrar->anterior != NULL) {
    (borrar->anterior)->siguiente =NULL;
    /*
     * No olvides la opreación de borrar el nodo que ya no quieres
     */
    free(borrar);
    return 0;
  }
  /*
   * Si la lista solo tiene un nodo, entonces simplemente reiniciamos
   * la lista.
   */
  inicio = NULL;
  free(borrar);
  return 0;
}

/*
 * Esta función muestra la lista Regresa 0 si fue existosa la
 * operación
 */
int64_t mostrar() {
  /*
   * Copia inicio en una posición auxiliar
   */  
  struct _nodo *aux = inicio;
  printf("La lista actualmente contiene:");
  /*
   * Recorre la lista uno a uno e imprime el dato
   */
  while(aux != NULL) {
    printf("%" PRId64",",aux->dato);
    aux = aux->siguiente;
  }
  /*
   * Solo para que exista un salto de línea, se quitaría si el
   * programa fuera interactivo
   */
  printf("\n");
  return 0;
}

int64_t main(int argc, char *argv[])
{
  /*
   * Inicio del apuntador global de inicio. no borres esto, lo demás
   * deberá sustituirse con tus scanf y esas cosas
   */
  inicio = NULL;

  borrarFinal();
  mostrar();
  insertaFinal(7);
  mostrar();
  borrarFinal();
  mostrar();
  insertaFinal(8);
  mostrar();
  insertaInicio(20);
  mostrar();
  insertaInicio(6);
  mostrar();
  insertaInicio(3);
  mostrar();
  borrarInicio();
  mostrar();
  insertaFinal(1);
  mostrar();
  insertaFinal(5);
  mostrar();
  borrarFinal();
  mostrar();
  return 0;
}
