#include <stdio.h>

int main(int argc, char *argv[])
{
  int i;
  printf("El número de argumentos: %i \n", argc);
  printf("El nombre del programa es: %s \n", argv[0]);
  printf("Los argumentos del programa son: ");
  for (i=1; i<argc; i++) {
    printf("%s ", argv[i]);
  }
  printf("\n");
  return 0;
}
